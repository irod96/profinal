from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User

from django import forms
from .forms import Cform, Cform2 , Cform3, Cform4

from .models import Kinders,Persona,Teachers, Padres

# Create your views here.
def index (request):
    return render(request,"home/CRUD_list.html",{})

def create(request):
    form = Cform(request.POST or None)
    message="Ingresa"
    if request.user.is_authenticated:
        message ="Acceso"
        if form.is_valid():
           instance = form.save(commit=False)
           form.save()
           return redirect('/')

        else:
           error = "log in"
    context ={"form":form,"message":error}
    return render(request,"home/create.html",context)

def create2(request):
    form = Cform2(request.POST or None)
    message="Ingresa"
    if request.user.is_authenticated:
        message ="Acceso"
        if form.is_valid():
           instance = form.save(commit=False)
           form.save()
           return redirect('/')

        else:
           error = "log in"
    context ={"form":form,"message":error}
    return render(request,"home/create.html",context)


def create3(request):
    form = Cform4(request.POST or None)
    message="Ingresa"
    if request.user.is_authenticated:
        message:"Acceso"
        if form.is_valid():
           instance = form.save(commit=False)
           form.save()
           return redirect("/")

        else:
            error = "log in"
    context ={"form":form,"message":error}
    return render(request,"home/create3.html",context)


def register(response):
    if response.method == "POST":
        form = UserCreationForm(response.POST)
        if form.is_valid():
            form.save()

        return redirect("/")
    else:
        form = UserCreationForm()
    return render(response,"home/register.html",{"form":form})

def lista(request):
    Kinder =Kinders.objects.all()
    return render(request,"home/here.html",{'Kinder':Kinder})


def lista2(request):
    Personaz =Persona.objects.all()
    return render(request,"home/here2.html",{'Personaz':Personaz})


def detail(request ,id):
    kinder= Kinders.objects.get(id=id)
    form =Cform(request.POST or None, instance=kinder)
    if form.is_valid():
        form.save()
        return redirect('lista')
    return render(request, "home/here4.html",{"form":form,"kinder":kinder})







def list(request):
    context= {
    "Kinders_list":Kinders.objects.all()
    }
    return render(request,"home/CRUD_lista.html",context)

def list2(request):
    context= {
    "Personas_list":Persona.objects.all()
    }
    return render(request,"home/detail.html",context)

def list3(request):
    context= {
    "Teachers_list":Teachers.objects.all()
    }
    return render(request,"home/detail2.html",context)

def lista4(request):
    context= {
    "Padre":Padres.objects.all()
    }
    return render(request,"home/Padre.html",context)




def showthis(request):
    users = User.objects.all()
    context ={"users":users}

    return render(request, 'home/CRUD_list.html', context)



def detail2(request ,id):
    queryset= Persona.objects.get(numero=id)
    context = {"object":queryset}
    return render (request,"home/personas_list.html",context)


def detail3(request ,id):
    queryset= Teachers.objects.get(teacher=id)
    context = {"object":queryset}
    return render (request,"home/teachers_list.html",context)


def detail22(request ,id):
    queryset= Padres.objects.get(id=id)
    context = {"object":queryset}
    return render (request,"home/padres_list.html",context)



def has_add_permission(self, request):
    return False

def has_delete_permission(self, request, obj=None):
    return False


def detail1(request ,id):
    queryset= Kinders.objects.get(id=id)
    context = {"object":queryset}
    return render (request,"home/kinders_list.html",context)

def update(request,id):
    kinder = Kinders.objects.get(id=id)
    if request.method=="GET":
        form=Cform(instance=kinder)
    else:
        form =Cform(request.POST,instance=kinder)
        if form.is_valid():
            form.save()
        return redirect('/')
    context={"form":form}
    return render(request,"home/update.html",context)


def delete(request,id):
    kinder = Kinders.objects.get(id=id)
    if request.method=="POST":
        kinder.delete()
        return redirect('/')
    context={"form":forms}
    return render(request,"home/delete.html",context)

def detail2(request ,id):
    queryset= Persona.objects.get(id=id)
    context = {"object":queryset}
    return render (request,"home/personas_list.html",context)


def update2(request,id):
    persona = Persona.objects.get(id=id)
    if request.method=="GET":
        form=Cform2(instance=persona)
    else:
        form =Cform2(request.POST,instance=persona)
        if form.is_valid():
            form.save()
        return redirect('/')
    context={"form":form}
    return render(request,"home/update2.html",context)

def update3(request,id):
    pad = Padres.objects.get(id=id)
    if request.method=="GET":
        form=Cform4(instance=pad)
    else:
        form =Cform4(request.POST,instance=pad)
        if form.is_valid():
            form.save()
        return redirect('/')
    context={"form":form}
    return render(request,"home/update3.html",context)


def delete2(request,id):
    persona = Persona.objects.get(id=id)
    if request.method=="POST":
        persona.delete()
        return redirect('/')
    context={"form":forms}
    return render(request,"home/delete2.html",context)



def delete3(request,id):
    p = Padres.objects.get(id=id)
    if request.method=="POST":
        p.delete()
        return redirect('/')
    context={"form":forms}
    return render(request,"home/delete3.html",context)
